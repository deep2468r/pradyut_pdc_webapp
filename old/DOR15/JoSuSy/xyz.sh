#!/bin/bash 

#SBATCH --job-name=job1
#SBATCH --partition=PRADYUT 
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=2
#SBATCH --time=00:10:00
#SBATCH -o slurm.%j.out
#SBATCH -e slurm.$j.err
kuberadir/share/installed-software/mpich-3.1/bin mpirun xhpl